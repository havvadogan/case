function NumberToCash(n) {
  return (
    parseFloat(n)
      .toFixed(2)
      .replace(/(\d)(?=(\d{3})+\.)/g, "$1.")
      .replace(/\.(\d+)$/, ",$1") + " TL "
  );
}

//create loading
document.getElementById("offer-container").innerHTML =
  "<div id='loading'><img src='img/loading.gif' /></div>";

async function getData() {
  const fetchNumberOffer = await fetch(
    "https://snetmyapp.herokuapp.com/get_offer_count"
  );
  let getOfferNumber = await fetchNumberOffer.json();
  getOfferNumber = getOfferNumber.num_offers;

  let offerList = [];
  for (let i = 0; i < getOfferNumber; i++) {
    const fetchData = await fetch("https://snetmyapp.herokuapp.com/case3");
    const response = await fetchData.json();
    offerList.push(response);
  }
  //loading remove
  document.getElementById("loading").remove();

  let cashSortBy = offerList.sort(function (a, b) {
    let cashA = a.Cash;
    let cashB = b.Cash;
    if (a.QuotaInfo.HasDiscount) {
      cashA = a.QuotaInfo.PremiumWithDiscount;
    }
    if (b.QuotaInfo.HasDiscount) {
      cashB = b.QuotaInfo.PremiumWithDiscount;
    }
    return cashA - cashB;
  });

  var offerCountHtml = document.createElement("h1");
  offerCountHtml.classList.add("title");
  offerCountHtml.innerHTML = `${getOfferNumber} sigorta teklifimiz var`;
  document.getElementById("offer-container").appendChild(offerCountHtml);

  let offerHtml = "";
  let popoverContent = "";
  let buttonHtml = "";

  cashSortBy.map((item) => {
    //cash control
    const hasDiscount = item.QuotaInfo.HasDiscount;

    //popover control
    if (item.popoverContent !== undefined) {
      popoverContent = `  
        <div class="popover-content">
            <span class="query-icon"></span>
            <span class="popover">
            <span class="popover__title">${
              item.popoverContent && item.popoverContent[0].Title
            }</span>
            <span class="popover__detail"
                >${
                  item.popoverContent && item.popoverContent[0].Description
                }</span>
            </span>
        </div>`;
    } else {
      popoverContent = "";
    }

    //button control
    if (!item.SaleClosed) {
      buttonHtml = `  
    <div class="offer__btn">
    <a href="javascript:;" class="btn">SATIN AL</a>
    </div>`;
    } else {
      buttonHtml = ` <div class="offer__btn">
    <a href="tel:444 24 00" class="btn btn--border">
      <small>TELEFONDA SATIN AL</small>
      <span>444 24 00</span>
    </a>
  </div>`;
    }

    offerHtml += `<div class="offer">
                    <div class="offer__content">
                    <div class="offer__left">
                        <div class="offer__img">
                             <img src=${item.ImagePath} alt=${item.FirmName}  />
                        </div>
                        <div class="offer__info">
                        <div class="offer__info__type"><strong>${
                          item.ProductDesc
                        }</strong></div>
                        <div class="offer__info__brand">
                            <div class="offer__info__brand__title">${
                              item.FirmName
                            }</div>
                            ${popoverContent}
                        </div>
                        </div>
                    </div>
                    <div class="offer__right">
                        <div class="offer__cash"  style=${
                          !hasDiscount && "display:none"
                        }>Peşin <span>${NumberToCash(item.Cash)}</span></div>
                        <div class="offer__cashOffer"><strong>${
                          hasDiscount === true
                            ? NumberToCash(item.QuotaInfo.PremiumWithDiscount)
                            : NumberToCash(item.Cash)
                        }</strong></div>
                    </div>
                    </div>
            
                   ${buttonHtml}
                </div>`;
  });

  document.getElementById("offer-container").innerHTML += offerHtml;
}

getData();
